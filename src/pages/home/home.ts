import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { LoginPage } from '../login/login';
import { NativeStorage } from '@ionic-native/native-storage';
import { ToastController } from 'ionic-angular';

// import { Geolocation } from '@ionic-native/geolocation';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  ligamentoAutomatico: any;
  alarmeDisparando: any;
  statusAlarme: any;
  valorLDR: any;
  portaoAberto: any;
  luzGaragem1 : any;
  luzGaragem2 : any;
  luzGaragem3 : any;
  luzGaragem4 : any;
  luzGaragem5 : any;
  latitude : any;
  longitude : any = "";
  repeticao : any = "";
  ativo: any = true;
  tempo: any;

  constructor(private toastCtrl: ToastController, public platform: Platform, private nativeStorage: NativeStorage, public navCtrl: NavController, public http: Http) {

    platform.ready().then(() => {
      this.atualizar();
      // this.repeticao = setInterval(() => {
      //   this.atualizar();
      // }, 3000);

      this.platform.pause.subscribe(() => {
          this.ativo = false;
      });

      this.platform.resume.subscribe(() => {
        // this.atualizar();
        this.ativo = true;
        this.timeout();
        // alert("Voltei");
      });

    });


  //   this.geolocation.getCurrentPosition().then((resp) => {
  //     if(this.latitude != resp.coords.latitude || this.longitude != resp.coords.longitude){
  //       var post = "latitude="+resp.coords.latitude+"&longitude="+resp.coords.longitude;
  //       var headers = new Headers();
  //       headers.append('Content-Type', 'application/x-www-form-urlencoded');
  //       this.http.post('http://', post, {
  //         headers: headers
  //       }).map(res => res.json())
  //       .subscribe(
  //         data => console.log(data),
  //         err => console.log(err),
  //       );
  //     }
  //     this.latitude = resp.coords.latitude;
  //     this.longitude = resp.coords.longitude;
  //   }).catch((error) => {
  //     console.log('Erro ao pegar localização', error);
  //   });
  //
  //   let watch = this.geolocation.watchPosition();
  //   watch.subscribe((data) => {
  //    // data can be a set of coordinates, or an error (if an error occurred).
  //    this.latitude = data.coords.latitude;
  //    this.longitude = data.coords.longitude;
  //   });
  }

  atualizar(){
    if(this.ativo){
      var post = "";
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post('http://precisionsistemas.homeip.net:8080', post, {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        data => this.preencher(data),
        err => {
          let toast = this.toastCtrl.create({
          message: 'Sem resposta',
          duration: 2000,
          position: 'middle',
        });
        toast.present();
        this.timeout();
        },
      );
    }
  }

  preencher(data){
    if(data.ligamentoAutomatico == "true"){
      this.ligamentoAutomatico = true;
    }else{
      this.ligamentoAutomatico = false;
    }
    if(data.alarmeDisparando == "true"){
      this.alarmeDisparando = true;
    }else{
      this.alarmeDisparando = false;
    }
    if(data.statusAlarme == "true"){
      this.statusAlarme = true;
    }else{
      this.statusAlarme = false;
    }

    if(data.status1 == "true"){
      this.luzGaragem1 = true;
    }else{
      this.luzGaragem1 = false;
    }

    if(data.status2 == "true"){
      this.luzGaragem2 = true;
    }else{
      this.luzGaragem2 = false;
    }

    if(data.status3 == "true"){
      this.luzGaragem3 = true;
    }else{
      this.luzGaragem3 = false;
    }

    if(data.status4 == "true"){
      this.luzGaragem4 = true;
    }else{
      this.luzGaragem4 = false;
    }

    if(data.status5 == "true"){
      this.luzGaragem5 = true;
    }else{
      this.luzGaragem5 = false;
    }

    this.valorLDR = data.valorLDR;
    if(data.portaoAberto == "false")
      this.portaoAberto = "ABERTO";
    else
      this.portaoAberto = "FECHADO";

    this.timeout();

    let toast = this.toastCtrl.create({
      message: 'Resposta do Arduino',
      duration: 1000,
      position: 'bottom',
    });
    toast.present();
  }

  timeout(){
    // if(this.ativo){
      this.tempo = setTimeout(()=>{
        this.atualizar()
      }, 1500);

      // let toast = this.toastCtrl.create({
      //   message: 'TimeOut iniciado',
      //   duration: 1000,
      //   position: 'top',
      // });
      // toast.present();
    // }

  }

  action(n){
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://precisionsistemas.homeip.net:8080?'+n, post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => {},
      err => alert("Não foi possível conectar com a placa."),
    );
  }

  sair(){
    this.nativeStorage.remove('login');
    this.navCtrl.setRoot(LoginPage);
  }
}
