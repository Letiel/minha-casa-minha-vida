import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import { HomePage } from '../home/home';

import {Http, Headers} from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  pin : any;
  loader: any;
  constructor(private nativeStorage: NativeStorage, public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController) {
    this.nativeStorage.getItem('login')
    .then(
      data => this.loginAuto(data),
    );
  }

  loginAuto(data){
    if(data.pin != "" && data.pin != null){
      this.pin = data.pin;
      this.entrar();
    }
  }

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    this.loader.present().catch(() => {});
  }

  carregou() {
    this.loader.dismiss().catch(() => {});
  }

  entrar(){
    this.carregando();
      var post = "pin=" + this.pin + "&token=token_nao_funciona";
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post('https://www.precisionsistemas.com.br/API/login_arduino', post, {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        data => this.processarLogin(data),
        err => {alert("Verifique sua conexão com a internet"); this.carregou();},
      );
  }

  processarLogin(data){
    if(data.logou == "true"){
      this.navCtrl.setRoot(HomePage, {});
      this.nativeStorage.setItem('login', {pin: this.pin});
    }else
      alert("PIN incorreto");
    this.carregou();
  }
}
